//Цикл foreEach это более короткая запись цикла for для массива, он проходит по каждому элементу,
//и таким образом можно получать, изменять или обрабатывать его элементы.
//
filterByForEach = (array, type) => {
    let result = [];
    array.forEach(function (item){
        if (typeof(item) !== typeof (type)){
            result.push(item)
        }
    })
    return result
}
console.log((filterByForEach(['hello', 'world', 23, '23', null],'string' )))

filterByFilter = (array, type) => {
    return array.filter(value => typeof value !== typeof type);
}
console.log(filterByFilter(['hello', 'world', 23, null],'string'))

